package main

import (
	"bytes"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/howeyc/gopass"
	"gitlab.com/starius/goathlib"
)

func generateFor(siteName string) error {
	var secret string
	for _, site := range store.Sites {
		if site.Name == siteName {
			secret = site.Secret
		}
	}
	if secret == "" {
		return fmt.Errorf("unknown site")
	}
	key, err := goathlib.ParseSecret(secret)
	if err != nil {
		return err
	}
	fmt.Println(goathlib.GenerateTOTP(key, time.Now()))
	return nil
}

func add() error {
	fmt.Printf("Enter the name of site: ")
	siteBytes, err := gopass.GetPasswdMasked()
	if err != nil {
		return err
	}
	siteName := string(bytes.TrimSpace(siteBytes))
	for _, site := range store.Sites {
		if site.Name == siteName {
			return fmt.Errorf("site already exists")
		}
	}
	if _, has := commands[siteName]; has {
		return fmt.Errorf("can not use command names as site names")
	}

	fmt.Printf("Enter the secret: ")
	secretBytes, err := gopass.GetPasswdMasked()
	if err != nil {
		return err
	}
	secret := string(secretBytes)
	if _, err := goathlib.ParseSecret(secret); err != nil {
		return err
	}

	store.Sites = append(store.Sites, Site{
		Name:   siteName,
		Secret: secret,
	})
	return saveStorage()
}

func rm() error {
	fmt.Printf("Enter the name of site: ")
	siteBytes, err := gopass.GetPasswdMasked()
	if err != nil {
		return err
	}
	siteName := string(bytes.TrimSpace(siteBytes))
	var newSites []Site
	for _, site := range store.Sites {
		if site.Name != siteName {
			newSites = append(newSites, site)
		}
	}
	if len(newSites) != len(store.Sites)-1 {
		return fmt.Errorf("unknown site")
	}
	store.Sites = newSites
	return saveStorage()
}

func ls() error {
	sites := make([]string, 0, len(store.Sites))
	for _, site := range store.Sites {
		sites = append(sites, site.Name)
	}
	sort.Strings(sites)
	fmt.Println(strings.Join(sites, " "))
	return nil
}

func secrets() error {
	for _, site := range store.Sites {
		fmt.Printf("%s\t%s\n", site.Name, site.Secret)
	}
	return nil
}

func exit() error {
	os.Exit(0)
	return nil
}

var commands map[string]func() error

func init() {
	commands = map[string]func() error{
		"add":     add,
		"rm":      rm,
		"ls":      ls,
		"secrets": secrets,
		"exit":    exit,
	}
}
