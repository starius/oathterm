package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/howeyc/gopass"
	tilde "gopkg.in/mattes/go-expand-tilde.v1"
)

var (
	store    *Storage
	password []byte
)

type Site struct {
	Name   string `json:"name"`
	Secret string `json:"secret"`
}

type Storage struct {
	Sites []Site `json:"sites"`
}

func saveStorage() error {
	encryptedData, err := marshalStorage(store, password)
	if err != nil {
		return err
	}
	storageFilename, err := tilde.Expand(*storage)
	if err != nil {
		log.Fatalf("Failed to expand filename: %v.", err)
	}
	if err := ioutil.WriteFile(storageFilename, encryptedData, 0600); err != nil {
		return fmt.Errorf("failed to write storage file %s: %v", storageFilename, err)
	}
	return nil
}

func readStorage() error {
	storageFilename, err := tilde.Expand(*storage)
	if err != nil {
		return fmt.Errorf("failed to expand filename: %v", err)
	}

	encryptedData, err := ioutil.ReadFile(storageFilename)
	if os.IsNotExist(err) {
		fmt.Printf("Creating encrypted secrets storage in %s. Please provide a password (can be an empty string): ", *storage)
		pass, err := gopass.GetPasswdMasked()
		if err != nil {
			return fmt.Errorf("password not entered: %v", err)
		}
		store = &Storage{}
		password = pass
		if err := saveStorage(); err != nil {
			return err
		}
		return nil
	}

	fmt.Printf("Please enter the password from %s: ", *storage)
	pass, err := gopass.GetPasswdMasked()
	if err != nil {
		return fmt.Errorf("password not entered: %v", err)
	}
	password = pass
	store, err = unmarshalStorage(encryptedData, password)
	return err
}
