module gitlab.com/starius/oathterm

go 1.19

require (
	github.com/golang/snappy v0.0.4
	github.com/howeyc/gopass v0.0.0-20210920133722-c8aef6fb66ef
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/starius/goathlib v0.0.0-20190712210817-9ea8d1e6d531
	gopkg.in/mattes/go-expand-tilde.v1 v1.0.0-20150330173918-cb884138e64c
)

require (
	golang.org/x/crypto v0.28.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/term v0.25.0 // indirect
)
