package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"fmt"

	"gitlab.com/NebulousLabs/fastrand"
)

func NewAEAD(key []byte) (cipher.AEAD, error) {
	h := sha256.Sum256(key)
	block, err := aes.NewCipher(h[:])
	if err != nil {
		return nil, fmt.Errorf("failed to make AES: %s", err)
	}
	aead, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("failed to make AEAD: %s", err)
	}
	return aead, nil
}

func encrypt(data, password []byte) ([]byte, error) {
	aead, err := NewAEAD(password)
	if err != nil {
		return nil, err
	}
	nonceSize := aead.NonceSize()
	nonce := fastrand.Bytes(nonceSize)
	encryptedData := aead.Seal(nil, nonce, data, nil)
	return append(nonce, encryptedData...), nil
}

func decrypt(encryptedData, password []byte) ([]byte, error) {
	aead, err := NewAEAD(password)
	if err != nil {
		return nil, err
	}
	nonceSize := aead.NonceSize()
	if len(encryptedData) < nonceSize {
		return nil, fmt.Errorf("the data is too short")
	}
	nonce := encryptedData[:nonceSize]
	return aead.Open(nil, nonce, encryptedData[nonceSize:], nil)
}
