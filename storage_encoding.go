package main

import (
	"encoding/json"

	"github.com/golang/snappy"
)

func marshalStorage(s *Storage, password []byte) ([]byte, error) {
	jsonData, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	snappyData := snappy.Encode(nil, jsonData)
	return encrypt(snappyData, password)
}

func unmarshalStorage(encryptedData []byte, password []byte) (*Storage, error) {
	snappyData, err := decrypt(encryptedData, password)
	if err != nil {
		return nil, err
	}
	jsonData, err := snappy.Decode(nil, snappyData)
	if err != nil {
		return nil, err
	}
	var s *Storage
	if err := json.Unmarshal(jsonData, &s); err != nil {
		return nil, err
	}
	return s, nil
}
