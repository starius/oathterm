package main

import (
	"flag"
	"fmt"
	"log"
	"sort"
	"strings"

	"github.com/howeyc/gopass"
)

var (
	storage = flag.String("storage", "~/.oathterm", "Encrypted file with stored secrets")
)

func main() {
	flag.Parse()

	if err := readStorage(); err != nil {
		log.Fatalf("Failed to open storage: %v.", err)
	}

	var prevSuccess string
	for {
		var commandNames []string
		for cmd := range commands {
			commandNames = append(commandNames, cmd)
		}
		sort.Strings(commandNames)
		fmt.Printf("Type the name of site or one of the commands: %s. Press Enter to repeat previous site: ", strings.Join(commandNames, ", "))
		inputBytes, err := gopass.GetPasswdMasked()
		if err != nil {
			fmt.Printf("Failed to enter command: %v.\n\n", err)
			continue
		}
		input := string(inputBytes)

		cmd, has := commands[input]
		if has {
			if err := cmd(); err != nil {
				fmt.Printf("Command %q failed: %v.\n\n", input, err)
			}
			continue
		}

		if input == "" {
			input = prevSuccess
		}

		if err := generateFor(input); err != nil {
			fmt.Printf("Failed to produce code for %q: %v.\n\n", input, err)
		} else {
			prevSuccess = input
		}
	}
}
